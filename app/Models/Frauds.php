<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Notifications\Notifiable;
use Nicolaslopezj\Searchable\SearchableTrait;
/**
 * Class Frauds
 * @package App\Models
 * @version March 11, 2020, 3:08 pm UTC
 *
 */
class Frauds extends Model
{
    use Notifiable;
    use SearchableTrait;

    public $table = 'frauds';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $searchable = [
        'columns' => [
            'frauds.phone1' => 10,
            'frauds.phone2' => 10,
        ]
    ];

    public $fillable = [
        'f_name',
        'description',
        'phone1',
        'phone2',
        'card1'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'f_name' => 'string',
        'description' => 'string',
        'phone1' => 'string',
        'phone2' => 'string',
        'card1' => 'string',
    ];
    public static function rules()
    {
        return [
            'f_name' => 'required',
            'description' => 'required',
            'phone1' => 'numeric|required',
        ];
    }
    protected $dates = [
        'created_at',
        'updated_at'
    ];



}
