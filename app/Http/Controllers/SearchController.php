<?php

namespace App\Http\Controllers;

use App\Models\Frauds;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        if($request->has('search')){
            $frauds = Frauds::search($request->get('search'))->get();
        }else{
            $frauds = Frauds::get();
        }


        return view('frauds.show', compact('frauds'));
    }
}
