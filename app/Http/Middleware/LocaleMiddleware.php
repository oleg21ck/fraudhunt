<?php

namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\App;
class LocaleMiddleware
{
    public function handle($request, Closure $next, $lang = null)
    {
        if ($lang) {
            App::setLocale($lang);
        }

        $response = $next($request);
        return $response->withCookie(cookie()->forever('lang', $lang));
    }

}

