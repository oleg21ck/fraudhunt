<?php

namespace App\Helpers;

use App\Repositories\LanguageRepository;
use Illuminate\Support\Facades\App;

class LanguageHelper
{
    public static function url ($link)
    {
        if (config('app.locale') != 'ru') {
            $link = config('app.locale') . $link;
        }

        return url($link);
    }

}
