import axios from 'axios';

const debounce = function (f, ms) {
    let timer = null;
    return function (...args) {
        const onComplete = () => {
            f.apply(this, args);
            timer = null;
        }
        if (timer) {
            clearTimeout(timer);
        }
        timer = setTimeout(onComplete, ms);
    };
}

export default {
    data: function () {
        return {
            keywords: '',
            searchArr: []
        }
    },
    watch: {
        keywords(after, before) {
            this.FetchData();
        }
    },
    methods: {
        FetchData: debounce(function () {
            if(!this.keywords) return this.searchArr = []
            axios.get('/api/search', {
                params: {
                    keywords: this.keywords
                }
            })
                .then(res => {
                    this.searchArr = res.data
                })
                .catch(err => {

                });
        }, 200)
    }
}
