<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    Route::get('locale/{locale}', function ($locale) {
        $previous_request = app('request')->create(url()->previous());
        $previous_route = app('router')
            ->getRoutes()
            ->match($previous_request)
            ->getName();

        $new_route = $locale . substr($previous_route, 2);

        return redirect()->route($new_route);
    })->name('locale');


    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('users', 'UserController');
    Route::resource('roles', 'RoleController');
    Route::get('captcha-form', 'CaptchaController@captchForm');
    Route::get('store-captcha-form', 'CaptchaController@storeCaptchaForm');


    Route::group(['middleware' => 'locale'], function () {
        Route::get('/', function () {
            return view('layouts/index');
        })->name('ru./');
        Auth::routes();
        Route::get('/advices', 'AdvicesController@index')->name('ru.advices');
        Route::get('/rules', 'RulesController@index')->name('ru.rules');
        Route::match(['get', 'post'], '/frauds/create', 'FraudsController@create')->name('ru.frauds/create');
        Route::match(['get', 'post'], '/contacts', 'ContactController@contact')->name('ru.contacts');
        Route::get('/frauds', 'FraudsController@index')->name('ru.frauds');
        Route::get('/frauds/index', 'SearchController@search')->name('ru.frauds/index');
    });

    Route::group(['prefix'=>'en', 'middleware' => 'locale:en'], function () {
        Route::get('/', function () {
            return view('layouts/index');
        })->name('en./');
        Auth::routes();
        Route::get('/advices', 'AdvicesController@index')->name('en.advices');
        Route::get('/rules', 'RulesController@index')->name('en.rules');
        Route::match(['get', 'post'], '/frauds/create', 'FraudsController@create')->name('en.frauds/create');
        Route::match(['get', 'post'], '/contacts', 'ContactController@contact')->name('en.contacts');
        Route::get('/frauds', 'FraudsController@index')->name('en.frauds');
        Route::get('/frauds/index', 'SearchController@search')->name('en.frauds/index');
    });

