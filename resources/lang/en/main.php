<?php

return [
    'send' => "Send",
    'menu' => [
        'home' => 'Home',
        'contacts' => 'Contacts',
        'rules' => 'Terms of Service',
        'frauds' =>'Frauds',
        'create_frauds' => 'Add frauds',
        'advices' => 'Useful tips',
        'register' => 'Registration',
        'login' => 'Login',
        'logout' => 'Logout',
    ],
    'index' => [
        'tag_title' => 'FRAUDHUNT',
        'msg' => 'All-Ukrainian scam information database',
        'create_frauds' => 'Report Scam',
        'advices' => 'Read the rules',
        'frauds' =>'Check in the database',
        'msg1' => '<h3>Answer the swindler correctly</h3>
                   <p>What can not be done when calling from an unknown number? How to behave in a telephone conversation with a potential crook?</p>
                   <p><strong>Check out the basic rules and tips for dealing with an unknown caller!</strong></p>',
        'msg2' => '<h3>Break through the base fraudster</h3>
                   <p>Are you planning to make a purchase online? Lost something and now they call you with offers to return the loss for money?</p>
                   <p><strong> Check out the FraudHunt database - don\'t fall prey to scammers!</strong></p><br/>',
        'msg3' => '<h3>Report scam</h3>
                   <p>I almost fell for scammers? Faced with cyber crooks who tried to breed you for money?</p>
                   <p><strong>Do a good deed, tell your friends and acquaintances about it! Help them not to be deceived by swindlers!</strong></p><br/>',
    ],
    'footer1' => 'By posting or using information in the "FraudHunt" database you confirm your agreement with ',
    'footer2' => '. All rights reserved. Any copying, publishing, reprinting of materials from the site is permitted provided that there is a direct indexable hyperlink to the "All-Ukrainian database of information about fraudsters "Fraud Hunt"".',
    'footer3' => 'the Terms of Service',
    'frauds' => [
        'tag_title' => 'Frauds',
        'msg1' => 'Now in the database ',
        'msg2' => ' fraud records (s)',
        'msg3' => 'To search, enter the full phone number (10 digits starting with "0")',
        'msg4' => 'Search',
        'msg5' => 'Reset',
        'msg6' => 'Add fraud',
    ],
    'advices' => [
        'tag_title' => 'Useful tips',
        'msg1' => '<h2>What to do if a friend asks for help through a social network?</h2>',
        'msg2' => ' <h3> Recently, cases of a new type of fraud through social networks have become more frequent - cloning a page (account). </h3>
                    <h4> Socialclone fraud scheme </h4>
                    <p> Fraudsters go to your page, download all your data: personal information, photos, videos, messages from the wall, etc. </p>
                    <p> Then create an exact copy (clone) of your page. </p>
                    <p> Then they go to your friends and ask to help out until tomorrow: to throw some amount onto a card or phone. Such a fraud scheme is especially relevant for active users of social networks, to those people who have a large number of friends and subscribers. </p> ',
        'msg3' => ' <h4> What should be alarming in the behavior of “your friend”? </h4>
                    <ol>
                        <li> Repeated request to add his or his friend as friends. </li>
                        <li> Please, under various pretexts, do something: give him your contacts (e-mail address, phone number), replenish his phone, bank card, etc. </li>
                        <li> A message about the discovery of your lost item or information compromising you. </li>
                    </ol>',
        'msg4' => ' <h4> Your actions if a friend asks for something through a social network </h4>
                    <ol>
                        <li> Be careful. </li>
                        <li> If this is not your friend, but ... a virtual friend from the social network, then it is better to ignore urgent requests. </li>
                        <li> If you know this person in real life, call him. There is no way to call, look at what is being done on his page in other social networks. Analyze, think. </li>
                        <li> If suspicious files (archives, files with extensions not known to you) are attached to the request, message of “Your Friend” - do not open them. If photos are attached, try finding duplicates using the <a href="http://www.google.com.ua/intl/en/insidesearch/features/images/searchbyimage.html"> Google service </a>. < / li>
                    </ol> ',
        'msg5' => 'Remember!',
        'msg6' => 'All information published by you on the Internet may be used against you.',
        'msg7' => ' <div class="col-lg-12">
                    <h2>What to do if you have been called and informed that your item has been found?</h2>
                </div>
                <div class="col-lg-11 col-lg-offset-1">
                    <h3>If you have lost any thing and you have been called to report a find, you need to know the basic rules when making a telephone conversation with a subscriber:</h3>
                    <span class="text-success">
                <h4>What you need to do:</h4>
                <ol>
                    <li> Ask where did you hear about your loss. </li>
                    <li> Make sure that this is really your thing by asking about the details and location of the find: ask, for example, the serial number of the document (do not ask for the name or date of your birth, because this information is easy to find using social networks), cover color, etc. . </li>
                    <li> If you think that this is still your thing, agree on a meeting place and conditions for its return. </li>
                </ol>
            </span>
                <span class="text-danger">
      <h4> What you don’t have to do: </h4>
                    <ol>
                        <li> Don’t agree to meet in less crowded places. </li>
                        <li> Do not transfer remuneration through a third party. </li>
                        <li> Do not call back to the number of the person who called you or any other number that may be offered to you. </li>
                        <li> Don\'t answer questions about your loss. </li>
                        <li> Do not take any action to transfer the reward until you receive your own lost thing. </li>
                        <li> If documents for a car or keys are lost, in no case do not go to a meeting on this vehicle, as this way a fraudster can find out additional information about your loss. </li>
                    </ol>
                </span>',
        'msg8' => 'A person who truly seeks to recover the loss to the owner will <strong> never raise the question of reward!</strong>',
        'msg9' => ' <div class="col-lg-12">
                    <h2>What to do if you have received an SMS with an offer to switch to a more favorable tariff?</h2>
                    </div>
                    <div class="col-lg-11 col-lg-offset-1">
                        <h3> An innocuous, at first glance, SMS with an offer to switch to a more favorable tariff may cause the resetting of your mobile account. It is precisely through such mailing that fraudsters often manage to improve their well-being. </h3>
                        <h4> How the circuit works. </h4>
                        <p> The subscriber, not paying attention to the fact that the sender of the messages is not their operator (it is often difficult to find out even intentionally), responds to the sentence: "Send SMS to a short number to switch to a more favorable tariff! </p>
                        <p> As a result of sending a message to the specified short number or even simply pressing the "Ok" button, just wanting to close the message, most likely the victim, that is, you will receive an SMS of this kind: "Congratulations, now all calls in the operator’s network are free!" . </p>
                        <p> You are happy, but not for long, because soon the real operator will inform you that there are less than 1 hryvnia on the account, or something like this, and you need to refill your account. </p>
                        <p> The result of your carelessness is "0" in the account and a deep sense of resentment. </p>
                        <p> The thing is that almost all operators offer a paid service - a short number that scammers use. </p>
                        <p> You can find out the cost of the service of sending SMS or a call to a short number on the websites of the operators:<a href="http://www.kyivstar.ua/f/1/mm/entertainment/partner_services/sms_services_rus.xls">for KyivStar subscribers </a> (table file * .xls), <a href="http://www.mts.ua/ru/support/services/8-kak-uznat-stoimot-zvonka-ili-sms-na-specialnyj-nomer-korotkij-nomer-informacionnye-sluzhby-kompanij-0-800/">for MTS subscribers </a> (table file * .xls)</p>
                        <div class="panel panel-info">
                            <div class="panel-heading">
                                <h3 class="panel-title"><h4>Recommendations:</h4></div>
                            <br>
                            <ol>
                                <li> Do not take action that you didn’t plan. </li>
                                <li> If the offer really seemed beneficial to you, double-check it on the offsite of your mobile operator. </li>
                            </ol>
                        </div>
                    </div>',
    ],
    'rules' => [
        'tag_title' => 'Terms of Service',
        'msg' => '<h4><strong>Posting scam information</strong></h4>
                <p>By posting information, you confirm your legal age and responsibility for posting the announcement. The information you post in the FraudHunt database should NOT contain:</p>
                <ul type="disc">
                    <li> vulgar, offensive language; </li>
                    <li> propaganda of hatred, violence, discrimination, racism, xenophobia, interethnic conflicts; </li>
                    <li> calls for violence and unlawful acts; </li>
                    <li> data that violates the personal rights or intellectual property rights of third parties; </li>
                    <li> information that promotes fraud, fraud or breach of trust; </li>
                    <li> information leading to transactions with stolen or fake items; </li>
                    <li> information that violates or infringes on the property of third parties, commercial secrets or the right to privacy; </li>
                    <li> personal or identifying information about other persons without the express consent of these persons; </li>
                    <li> information containing information infringing on privacy, insulting someone’s honor, dignity or business reputation; </li>
                    <li> information containing defamation or threats against anyone; </li>
                    <li> pornographic information; </li>
                    <li> Information harmful to minors; </li>
                    <li> false and misleading information; </li>
                    <li> viruses or any other technology that could harm websites, companies or other users; </li>
                    <li> information about services deemed immoral, such as: prostitution, usury or other forms that are contrary to moral or legal standards; </li>
                    <li> links or information about sites competing with the company\'s services; </li>
                    <li> information representing “spam,” “letters of happiness,” “pyramid schemes,” or unwanted or false commercial advertising; </li>
                    <li> information distributed by news agencies; </li>
                    <li> information with the offer of earnings on the Internet, without specifying the physical address and direct contacts of the employer; </li>
                    <li> information with a franchise offer, multi-level and network marketing, agent activities, sales representation or any other activity that requires the recruitment of other members, subagents, sub-distributors, etc .; </li>
                    <li> information exclusively advertising without the offer of a specific product or service; </li>
                    <li> information that otherwise violates the law of the jurisdiction for which the announcement is intended. </li>
                </ul>
                <h4>Limitation of liability</h4>
                <p>None of the authors, participants, sponsors, administrators, operators and persons in any way associated with the information web resource &#171;All-Ukrainian database of information about fraudsters "Fraud Hunt"&#187;, is not responsible for the appearance of inaccurate or false information, as well as for your use of data located on these web pages or found by links from them; under no circumstances shall be liable to any party for any direct, indirect, special or other indirect damage resulting from any use of information on this site or on any other site to which there are links from this site, including any loss of profit , termination of business, loss of programs or data in your information systems or otherwise, even if the site administration is clearly notified of the possibility of such damage.</p>',
    ],
    'login' => [
        'tag_title' => 'login',
        'remember_me' => 'Remember me',
        'forgot_your_password' => 'If you have forgotten your password, you can change it.',
        'msg' => 'Please fill in all the fields for entry:',
        'fields' => [
            'email' => [
                'label' => "Email",
                'placeholder' => "email",
            ],
            'password' => [
                'label' => "Password",
                'placeholder' => "********",
            ],
        ]
    ],
    'create' => [
        'tag_title' => 'Add frauds',
        'msg' => 'Add frauds',
        'fields' => [
            'f_name' => [
                'label' => "Full name",
                'placeholder' => "Ivanov Ivan Ivanovich",
            ],
            'description' => [
                'label' => "More details",
                'placeholder' => "More details",
            ],
            'phone1' => [
                'label' => "Phone №1",
                'placeholder' => "0930000000",
            ],
            'phone2' => [
                'label' => "Phone №2",
                'placeholder' => "0670000000",
            ],
            'card1' => [
                'label' => "Card number",
                'placeholder' => "0000000000000000",
            ]
        ]
    ],
    'contact' => [
        'tag_title' => 'Contacts',
        'meta_description' => 'Contacts',
        'header' => 'Contacts',
        'sent' => 'Your message was successfully sent to the email',
        'msg1' => 'If you have a business proposal or other questions, please fill out the form. Thanks.',
        'fields' => [
            'full_name' => [
                'label' => "Full name",
                'placeholder' => "Ivanov Ivan Ivanovich",
            ],
            'email' => [
                'label' => "Email",
                'placeholder' => "example@mail.com",
            ],
            'topic' => [
                'label' => "Topic",
                'placeholder' => "Topic",
            ],
            'body' => [
                'label' => "Message",
                'placeholder' => "Message text",
            ],
        ]
    ],
    'register' => [
        'tag_title' => 'Registration',
        'msg' => 'Please fill in all the fields for registration:',
        'fields' => [
            'name' => [
                'label' => "Username",
                'placeholder' => "Username",
            ],
            'email' => [
                'label' => "Email",
                'placeholder' => "example@mail.com",
            ],
            'password' => [
                'label' => 'Password',
                'placeholder' => "********",
            ],
            'confirm_password' => [
                'label' => 'Confirm password',
                'placeholder' => "********",
            ],
        ]
    ],
    'search' => [
        'fields' => '<th>Full Name</th>
            <th>More details</th>
            <th>Phone №1</th>
            <th>Phone №2</th>
            <th>Card number</th>',
        'res' => 'Searching results',
        'err' => 'Result not found.',
        'but' => 'Search',
        'place' => 'Enter phone number ХХХХХХХХХХ',
        'back' => 'Back',
        'results1' => 'Found for your request',
        'results2' => 'fraud records (s)'
    ]
];
