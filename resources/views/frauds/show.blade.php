@extends('layouts.app')
@section('content')
    <div class="container">
        <ul class="breadcrumb"><li><a href="{{ \App\Helpers\LanguageHelper::url('/') }}">@lang('main.menu.home')</a></li>
            <li><a href="{{  \App\Helpers\LanguageHelper::url('/frauds') }}">@lang('main.menu.frauds')</a></li>
            <li class="active">@lang('main.search.res')</li>
        </ul>
        <h2>
            @lang('main.search.results1')
                {{ $frauds->count() }}
            @lang('main.search.results2')
        </h2>
    <table class="table table-bordered">
        <tr>
            @lang('main.search.fields')
        </tr>

        @if($frauds->count())
            @foreach($frauds as $fraud)
                <tr>
                    <td>{{ $fraud->f_name }}</td>
                    <td>{{ \Illuminate\Support\Str::limit($fraud->description) }}</td>
                    <td>{{ $fraud->phone1 }}</td>
                    <td>{{ $fraud->phone2 }}</td>
                    <td>{{ $fraud->card1 }}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="5" class="text-danger">@lang('main.search.err')</td>
            </tr>
        @endif

    </table>
        <a href="{{ \App\Helpers\LanguageHelper::url('/frauds') }}" class="btn btn-default">@lang('main.search.back')</a>
    </div>
@endsection
