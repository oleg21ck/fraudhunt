@extends('layouts.app')
@section('title', __("main.frauds.tag_title"))
@section('content')
<div class="container">
    <div class="container">
        <ul class="breadcrumb"><li><a href="{{ \App\Helpers\LanguageHelper::url('/') }}">@lang('main.menu.home')</a></li>
            <li class="active">@lang('main.menu.frauds')</li>
        </ul>
    </div>
    <div class="container fraud-index">
        <h1>
            @lang('main.frauds.msg1')
            @include('frauds.table')
            @lang('main.frauds.msg2')
        </h1>
        <div class="container">
            <div class="row">
                <form method="GET" action="{{ \App\Helpers\LanguageHelper::url('/frauds/index') }}">
                    <div class="row">
                        <div class="input-group_search">
                            <input type="number" name="search" class="form-control_search" maxlength="15" placeholder="@lang('main.search.place')">
                            <div class="input-group-btn">
                                <button class="btn btn-primary">@lang('main.search.but')</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <p><a class="btn btn-success" href="{{ \App\Helpers\LanguageHelper::url('/frauds/create') }}">@lang('main.frauds.msg6')</a></p>
        </div>
    </div>
</div>
@endsection
