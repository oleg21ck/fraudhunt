<div class="form-group">
    {!! Form::label('f_name', 'Full name') !!}
    <p>{{ $frauds->f_name }}</p>
</div>

<div class="form-group">
    {!! Form::label('description', 'Описание:') !!}
    <p>{{ $frauds->description }}</p>
</div>

<div class="form-group">
    {!! Form::label('phone1', 'Phone1:') !!}
    <p>{{ $frauds->phone1 }}</p>
</div>

<div class="form-group">
    {!! Form::label('phone2', 'Phone2:') !!}
    <p>{{ $frauds->phone2 }}</p>
</div>

<div class="form-group">
    {!! Form::label('card1', 'Card1:') !!}
    <p>{{ $frauds->card1 }}</p>
</div>
