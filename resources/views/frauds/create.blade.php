@extends('layouts.app')
@section('title', __('main.create.tag_title'))
@section('content')
<div class="container">
    <div class="container">
        <ul class="breadcrumb"><li><a href="{{ \App\Helpers\LanguageHelper::url('/') }}">@lang('main.menu.home')</a></li>
            <li><a href="{{  \App\Helpers\LanguageHelper::url('/frauds') }}">@lang('main.menu.frauds')</a></li>
            <li class="active">@lang('main.menu.create_frauds')</li>
        </ul>
    </div>
    <div class="container site-contact">
        <section class="content-header">
            <h1>
                @lang('main.create.msg')
            </h1>
        </section>

        @php($attributes = ['class'=> 'form-control',])
        @php($attributes_1 = ['class'=> 'form-control_1',])
        @php($attributes_label = ['class'=> 'control-label'])
        <div class="container">
            <div class="row">
                <div class="col-lg-5">
                    {!! Form::open(['url' => ('/frauds/create'), 'method' => 'post', 'role' => 'form', 'class' => 'form-horizontal']) !!}
                        @include('frauds.fields')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
