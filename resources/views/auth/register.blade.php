@extends('layouts.app')
@section('title', __("main.register.tag_title"))
@section('content')
<div class="container">
    <div class="container">
        <ul class="breadcrumb"><li><a href="{{ \App\Helpers\LanguageHelper::url('/') }}">@lang('main.menu.home')</a></li>
            <li class="active">@lang('main.menu.register')</li>
        </ul>
    </div>
    <div class="container site-login">
        <h1>@lang('main.menu.register')</h1>
        <p>@lang('main.register.msg')</p>
        @php($attributes = ['class'=> 'form-control', 'required'])
        @php($attributes_label = ['class'=> 'control-label'])
        <div class="row">
            <div class="col-lg-5">
                {!! Form::open(['url' => url('/register'), 'method' => 'post', 'class' => 'form-horizontal', 'role' => 'form', 'id' => 'registration']) !!}

                    <div class="form-group {{ $errors->has('name') ? ' has-error' : '' }}">
                        {{ Form::label('name', __('main.register.fields.name.label'), $attributes_label) }}
                        {{ Form::text('name', old('name'), array_merge($attributes, ['autofocus', 'placeholder' => __('main.register.fields.name.placeholder')])) }}
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        {{ Form::label('email', __('main.register.fields.email.label'), $attributes_label) }}
                        {{ Form::email('email', old('email'), array_merge($attributes, ['autofocus', 'placeholder' => __('main.register.fields.email.placeholder')])) }}
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                        {{ Form::label('password', __('main.register.fields.password.label'), $attributes_label) }}
                        {{ Form::password('password', array_merge($attributes, ['autofocus', 'placeholder' => __('main.register.fields.password.placeholder')])) }}
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        {{ Form::label('password_confirmation', __('main.register.fields.confirm_password.label'), $attributes_label) }}
                        {{ Form::password('password_confirmation', array_merge($attributes, ['autofocus', 'placeholder' => __('main.register.fields.confirm_password.placeholder')])) }}
                    </div>

                    <div class="form-group">
                        {!! NoCaptcha::renderJs() !!}
                        {!! NoCaptcha::display() !!}
                        <span class="text-danger">{{ $errors->first('g-recaptcha-response') }}</span>
                    </div>

                    <div class="form-group">
                        {{ Form::submit(__('main.menu.register'), ['class' => 'btn btn-primary btn-wide middle-button']) }}
                    </div>
                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
