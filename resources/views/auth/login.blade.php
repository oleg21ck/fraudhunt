@extends('layouts.app')
@section('title', __("main.login.tag_title"))
@section('content')
<div class="container">
    <div class="container">
        <ul class="breadcrumb"><li><a href="/">@lang('main.menu.home')</a></li>
            <li class="active">@lang('main.menu.login')</li>
        </ul>
    </div>
    <div class="container site-login">
        <h1>@lang('main.menu.login')</h1>
        <p>@lang('main.login.msg')</p>
        @php($attributes = ['class'=> 'form-control', 'required'])
        @php($attributes_label = ['class'=> 'control-label'])
            <div class="row">
                <div class="col-lg-5">
                    {!! Form::open(['url' => url('/login'), 'method' => 'post', 'role' => 'form']) !!}
                            <div class="form-group text{{ $errors->has('email') ? ' has-error' : '' }}">
                                {{ Form::label('email', __('main.login.fields.email.label'), $attributes_label) }}
                                {{ Form::email('email', old('email'), ['class' => 'form-control', 'required', 'autofocus', 'autocomplete' => 'email']) }}

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group text{{ $errors->has('password') ? ' has-error' : '' }}">
                                {{ Form::label('password', __('main.login.fields.password.label'), $attributes_label) }}
                                {{ Form::password('password', ['class' => 'form-control', 'required']) }}

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            {{ Form::submit(__('Вход'), ['class' => 'btn btn-primary btn-wide middle-button']) }}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
    </div>
</div>
@endsection
