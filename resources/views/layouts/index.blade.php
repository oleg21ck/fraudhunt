@extends('layouts.app')
@section('title', __('main.index.tag_title'))
@section('content')
        <div class="site-index">
            <div class="padding-top padding-bottom align-center backgound-red">
                <img src="{{ url('images/StopFraud_960x432.png') }}" alt="">
                    <h2 style="text-align: center;color: white">@lang('main.index.msg')</h2>
                <a class="btn btn-danger button-big" href="{{ \App\Helpers\LanguageHelper::url('/frauds/create') }}">@lang('main.index.create_frauds')</a>        <br/>
            </div>
            <div class="body-content">
                <div class="container">
                    <div class="row padding-top padding-bottom">
                        <div class="col-lg-4 align-center">
                            <a href="{{ \App\Helpers\LanguageHelper::url('/advices') }}"><img src="{{ url('images/StopFraudFinger0_600-300x300.png') }}" alt=""></a>
                                @lang('main.index.msg1')
                            <a class="btn btn-danger button-big" href="{{ \App\Helpers\LanguageHelper::url('/advices') }}">@lang('main.index.advices')</a>
                        </div>
                        <div class="col-lg-4 align-center">
                            <a href="{{ \App\Helpers\LanguageHelper::url('/frauds') }}"><img src="{{ url('images/StopFraudFinger1_600-300x300.png') }}" alt=""></a>
                                @lang('main.index.msg2')
                            <a class="btn btn-danger button-big" href="{{ \App\Helpers\LanguageHelper::url('/frauds') }}">@lang('main.index.frauds')</a>
                        </div>
                        <div class="col-lg-4 align-center">
                            <a href="{{ \App\Helpers\LanguageHelper::url('/frauds/create') }}"><img src="{{ url('images/StopFraudFinger3_600-300x300.png') }}" alt=""></a>
                                @lang('main.index.msg3')
                            <a class="btn btn-danger button-big" href="{{ \App\Helpers\LanguageHelper::url('/frauds/create') }}">@lang('main.index.create_frauds')</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
