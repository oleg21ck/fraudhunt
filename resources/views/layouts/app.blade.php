<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="{{ url('css/app.css') }}" rel="stylesheet">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ url('images/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ url('images/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ url('images/favicon-16x16.png') }}">
    @yield('css')
</head>
<body>
<div id="app" class="wrap">
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{ \App\Helpers\LanguageHelper::url('/') }}"><img src="{{ url('images/FraudHunt_Logo_160x55_5.png') }}" width="25" alt=""></a>
            </div>
            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{{ \App\Helpers\LanguageHelper::url('/') }}">FRAUDHUNT</a></li>
                </ul>
                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    @include('layouts.menu')
                </ul>
            </div>
        </div>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                @yield('content')
            </div>
        </div>
    </div>

    <footer>
        <div class="footer">
            <div class="align-center padding-bottom">
                <p>
                    @lang('main.footer1')<a href="{{ \App\Helpers\LanguageHelper::url('/rules') }}">@lang('main.footer3')</a>@lang('main.footer2')
                </p>
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <p class="pull-left">&copy; Klevrit 2020</p>
                    <p class="pull-right">Powered by<a href="https://laravel.com"> Laravel</a></p>
                </div>
            </div>
        </div>
    </footer>
</div>
    {!! Html::script(mix('js/app.js')) !!}
    {!! Html::script(mix('js/manifest.js')) !!}
    {!! Html::script(mix('js/vendor.js')) !!}
    {!! Html::script(mix('js/full.js')) !!}
    @stack('scripts')
</body>
</html>
