
    <li class="dropdown"><a class="dropdown-toggle" href="{{ \App\Helpers\LanguageHelper::url('/') }}" data-toggle="dropdown">@lang('main.menu.home') <b class="caret"></b></a>
        <ul id="w3" class="dropdown-menu">
            <li><a href="{{ \App\Helpers\LanguageHelper::url('/contacts') }}">@lang('main.menu.contacts')</a></li>
            <li><a href="{{ \App\Helpers\LanguageHelper::url('/rules') }}" class="border_1">@lang('main.menu.rules')</a></li>
        </ul>
    </li>
    <li><a href="{{ \App\Helpers\LanguageHelper::url('/frauds') }}">@lang('main.menu.frauds')</a></li>
    <li><a href="{{ \App\Helpers\LanguageHelper::url('/frauds/create') }}">@lang('main.menu.create_frauds')</a></li>
    <li><a href="{{ \App\Helpers\LanguageHelper::url('/advices') }}">@lang('main.menu.advices')</a></li>
    <!-- Authentication Links -->
    @if($user = Auth::user())
        <li><a href="{{ \App\Helpers\LanguageHelper::url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">@lang('main.menu.logout') ({{$user->name}})</a></li>
        {!! Form::open(['url' =>  url('/logout'), 'method' => 'post', 'style' => 'display:none', 'id' => 'logout-form']) !!}
        {!! Form::close() !!}
    @else
        <li><a href="{{ \App\Helpers\LanguageHelper::url('/register') }}">@lang('main.menu.register')</a></li>
        <li><a href="{{ \App\Helpers\LanguageHelper::url('/login') }}">@lang('main.menu.login')</a></li>
    @endif

    <li><a class="icon" href="{{ route('locale', ['locale' => 'en']) }}">
        <img src="{{ url('images/en.png') }}" alt="English" title="English" width="32" height="25"></a></li>
    <li><a class="icon" href="{{ route('locale', ['locale' => 'ru']) }}">
        <img src="{{ url('images/ru.png') }}" alt="Русский" title="Russian" width="32" height="25"></a></li>



