@extends('layouts.app')
@section('title', __('main.advices.tag_title'))
@section('content')
    <div class="container">
        <div class="container">
            <ul class="breadcrumb"><li><a href="{{ \App\Helpers\LanguageHelper::url('/') }}">@lang('main.menu.home')</a></li>
                <li class="active">@lang('main.menu.advices')</li>
            </ul>
        </div>
        <div class="container site-advices">
            <h1>@lang('main.menu.advices')</h1>
            <div class="row">
                <div class="col-lg-12">
                    @lang('main.advices.msg1')
                </div>
                <div class="col-lg-11 col-lg-offset-1">
                        @lang('main.advices.msg2')
                    <span class="text-danger">
                        @lang('main.advices.msg3')
                    </span>
                    <span class="text-success">
                        @lang('main.advices.msg4')
                    </span>
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">@lang('main.advices.msg5')</h3>
                        </div>
                        <div class="panel-body">
                            @lang('main.advices.msg6')
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                @lang('main.advices.msg7')
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">@lang('main.advices.msg5')</h3>
                        </div>
                        <div class="panel-body">
                            @lang('main.advices.msg8')
                        </div>
                    </div>
            </div>
        </div>

            <div class="row">
                @lang('main.advices.msg9')
            </div>
    </div>
@endsection
