@extends('layouts.app')
@section('title', __("main.rules.tag_title"))
@section('content')
    <div class="container">
        <div class="container">
            <ul class="breadcrumb"><li><a href="{{ \App\Helpers\LanguageHelper::url('/') }}">@lang('main.menu.home')</a></li>
                <li class="active">@lang('main.menu.rules')</li>
            </ul>
        </div>
        <div class="container site-contact">
            <h1>@lang('main.menu.rules')</h1>

            <div class="row">
                <div class="col-lg-12">

                    @lang('main.rules.msg')
                </div>
            </div>
        </div>
    </div>
@endsection
