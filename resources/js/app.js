require('./bootstrap');

window.Vue = require('vue');

Vue.component('intl', require('./components/intl').default);

const app = new Vue({
    el: '#app'
});

