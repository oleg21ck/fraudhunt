const mix = require('laravel-mix');

mix.js('resources/js/app.js', 'public/js')
    .version()
    .extract([
        'vue'
    ])
    .sass('resources/sass/app.scss', 'public/css');

mix.styles([
    'resources/front/css/bootstrap.css',
    'resources/front/css/site.css',

], 'public/css/app.css');

mix.scripts([
    'resources/front/js/jquery.js',
    'resources/front/js/bootstrap.js',
    'resources/front/js/vue.min.js',
    'resources/front/js/vue-resource.min.js',
    'resources/front/js/yii.js'
], 'public/js/full.js');

mix.options({
    terser: {
        extractComments: false,
    }
});
